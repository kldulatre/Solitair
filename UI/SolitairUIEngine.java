import java.util.*;
import java.util.Scanner;
import java.io.*;

public class SolitairUIEngine{
	int[] deck;
	int[] A,B,C,D,E,F;
	int[] S1,S2,S3,S4,S5,S6,S7;
	int s1,s2,s3,s4,s5,s6,s7;

	public SolitairUIEngine(){
		this.newGame();
	}

	public void newGame(){
		this.deck = new int[0];
		for(int x=1;x<=52;x++){
			this.deck = Arrays.copyOf(this.deck,this.deck.length+1);
			this.deck[this.deck.length-1] = x;
		}
		this.A = this.B = this.C = this.D = this.E = this.F = new int[0];
		this.S1 = this.S2 = this.S3 = this.S4 = this.S5 = this.S6 = this.S7 = new int[0];
		this.s1 = this.s2 = this.s3 = this.s4 = this.s5 = this.s6 = this.s7 = 0;
		this.shuffleCards();
		this.distributeCards();
	}

	public void shuffleCards(){
		Random random = new Random();
		for(int x=0;x<52;x++){
			this.swap(random.nextInt(52),random.nextInt(52));
		}
	}

	public void swap(int x, int y){
		Integer temp = deck[x];
		deck[x] = deck[y];
		deck[y] = temp;
	}

	public void distributeCards(){
		for(int x=0;x<52;x++){
			if(x<1) this.addCard(1,this.deck[x]);
			else if(x<3) this.addCard(2,this.deck[x]);
			else if(x<6) this.addCard(3,this.deck[x]);
			else if(x<10) this.addCard(4,this.deck[x]);
			else if(x<15) this.addCard(5,this.deck[x]);
			else if(x<21) this.addCard(6,this.deck[x]);
			else if(x<28) this.addCard(7,this.deck[x]);
			else this.addCard('F',this.deck[x]);
		}
	}

	public void addCard(int stack,int value){
		if(stack==1){
			this.S1 = Arrays.copyOf(this.S1, this.S1.length + 1);
			this.S1[this.S1.length - 1] = value;
		}
		else if(stack==2){
			this.S2 = Arrays.copyOf(this.S2, this.S2.length + 1);
			this.S2[this.S2.length - 1] = value;	
		}
		else if(stack==3){
			this.S3 = Arrays.copyOf(this.S3, this.S3.length + 1);
			this.S3[this.S3.length - 1] = value;	
		}
		else if(stack==4){
			this.S4 = Arrays.copyOf(this.S4, this.S4.length + 1);
			this.S4[this.S4.length - 1] = value;	
		}
		else if(stack==5){
			this.S5 = Arrays.copyOf(this.S5, this.S5.length + 1);
			this.S5[this.S5.length - 1] = value;	
		}
		else if(stack==6){
			this.S6 = Arrays.copyOf(this.S6, this.S6.length + 1);
			this.S6[this.S6.length - 1] = value;	
		}
		else if(stack==7){
			this.S7 = Arrays.copyOf(this.S7, this.S7.length + 1);
			this.S7[this.S7.length - 1] = value;	
		}
	}

	public void addCard(int stack,int value,boolean pass){
		if(stack==1){
			this.S1 = Arrays.copyOf(this.S1, this.S1.length + 1);
			this.S1[this.S1.length - 1] = value;
		}
		else if(stack==2){
			this.S2 = Arrays.copyOf(this.S2, this.S2.length + 1);
			this.S2[this.S2.length - 1] = value;	
		}
		else if(stack==3){
			this.S3 = Arrays.copyOf(this.S3, this.S3.length + 1);
			this.S3[this.S3.length - 1] = value;	
		}
		else if(stack==4){
			this.S4 = Arrays.copyOf(this.S4, this.S4.length + 1);
			this.S4[this.S4.length - 1] = value;	
		}
		else if(stack==5){
			this.S5 = Arrays.copyOf(this.S5, this.S5.length + 1);
			this.S5[this.S5.length - 1] = value;	
		}
		else if(stack==6){
			this.S6 = Arrays.copyOf(this.S6, this.S6.length + 1);
			this.S6[this.S6.length - 1] = value;	
		}
		else if(stack==7){
			this.S7 = Arrays.copyOf(this.S7, this.S7.length + 1);
			this.S7[this.S7.length - 1] = value;	
		}

		if(stack==1 && pass==true) this.s1++;
		else if(stack==2 && pass==true) this.s2++;
		else if(stack==3 && pass==true) this.s3++;
		else if(stack==4 && pass==true) this.s4++;
		else if(stack==5 && pass==true) this.s5++;
		else if(stack==6 && pass==true) this.s6++;
		else if(stack==7 && pass==true) this.s7++;
	}

	public void addCard(char stack,int value){
		if(stack=='A'){
			this.A = Arrays.copyOf(this.A, this.A.length + 1);
			this.A[this.A.length - 1] = value;	
		}
		else if(stack=='B'){
			this.B = Arrays.copyOf(this.B, this.B.length + 1);
			this.B[this.B.length - 1] = value;
		}
		else if(stack=='C'){
			this.C = Arrays.copyOf(this.C, this.C.length + 1);
			this.C[this.C.length - 1] = value;	
		}
		else if(stack=='D'){
			this.D = Arrays.copyOf(this.D, this.D.length + 1);
			this.D[this.D.length - 1] = value;	
		}
		else if(stack=='E'){
			this.E = Arrays.copyOf(this.E, this.E.length + 1);
			this.E[this.E.length - 1] = value;
		}
		else if(stack=='F'){
			this.F = Arrays.copyOf(this.F, this.F.length + 1);
			this.F[this.F.length - 1] = value;
		}
	}

	public void removeCard(int stack){
		if(stack==1) this.S1  = Arrays.copyOf(this.S1,this.S1.length-1);
		else if(stack==2) this.S2  = Arrays.copyOf(this.S2,this.S2.length-1);
		else if(stack==3) this.S3  = Arrays.copyOf(this.S3,this.S3.length-1);
		else if(stack==4) this.S4  = Arrays.copyOf(this.S4,this.S4.length-1);
		else if(stack==5) this.S5  = Arrays.copyOf(this.S5,this.S5.length-1);
		else if(stack==6) this.S6  = Arrays.copyOf(this.S6,this.S6.length-1);
		else if(stack==7) this.S7  = Arrays.copyOf(this.S7,this.S7.length-1);
	}

	public void removeCard(int stack, boolean pass){
		if(stack==1) this.S1  = Arrays.copyOf(this.S1,this.S1.length-1);
		else if(stack==2) this.S2  = Arrays.copyOf(this.S2,this.S2.length-1);
		else if(stack==3) this.S3  = Arrays.copyOf(this.S3,this.S3.length-1);
		else if(stack==4) this.S4  = Arrays.copyOf(this.S4,this.S4.length-1);
		else if(stack==5) this.S5  = Arrays.copyOf(this.S5,this.S5.length-1);
		else if(stack==6) this.S6  = Arrays.copyOf(this.S6,this.S6.length-1);
		else if(stack==7) this.S7  = Arrays.copyOf(this.S7,this.S7.length-1);
	
		if(stack==1 && pass==true) this.s1--;
		else if(stack==2 && pass==true) this.s2--;
		else if(stack==3 && pass==true) this.s3--;
		else if(stack==4 && pass==true) this.s4--;
		else if(stack==5 && pass==true) this.s5--;
		else if(stack==6 && pass==true) this.s6--;
		else if(stack==7 && pass==true) this.s7--;
	}

	public void removeCard(char stack){
		if(stack=='A') this.A  = Arrays.copyOf(this.A,this.A.length-1);
		else if(stack=='B') this.B  = Arrays.copyOf(this.B,this.B.length-1);
		else if(stack=='C') this.C  = Arrays.copyOf(this.C,this.C.length-1);
		else if(stack=='D') this.D  = Arrays.copyOf(this.D,this.D.length-1);
		else if(stack=='E') this.E  = Arrays.copyOf(this.E,this.E.length-1);
		else if(stack=='F') this.F  = Arrays.copyOf(this.F,this.F.length-1);
	}

	public int getCard(int stack){
		if(stack==1) return this.S1[this.S1.length-1];
		else if(stack==2) return this.S2[this.S2.length-1];
		else if(stack==3) return this.S3[this.S3.length-1];
		else if(stack==4) return this.S4[this.S4.length-1];
		else if(stack==5) return this.S5[this.S5.length-1];
		else if(stack==6) return this.S6[this.S6.length-1];
		else if(stack==7) return this.S7[this.S7.length-1];
		else return 0;
	}

	public int getCard(int stack,int count){
		if(stack==1) return this.S1[this.S1.length-count];
		else if(stack==2) return this.S2[this.S2.length-count];
		else if(stack==3) return this.S3[this.S3.length-count];
		else if(stack==4) return this.S4[this.S4.length-count];
		else if(stack==5) return this.S5[this.S5.length-count];
		else if(stack==6) return this.S6[this.S6.length-count];
		else if(stack==7) return this.S7[this.S7.length-count];	
		else return 0;
	}

	public int getCard(char stack){
		if(stack=='A') return this.A[this.A.length-1];
		else if(stack=='B') return this.B[this.B.length-1];
		else if(stack=='C') return this.C[this.C.length-1];
		else if(stack=='D') return this.D[this.D.length-1];
		else if(stack=='E') return this.E[this.E.length-1];
		else if(stack=='F') return this.F[this.F.length-1];
		else return 0;
	}

	public int getCard(char stack,int count){
		if(stack=='A') return this.A[this.A.length-count];
		else if(stack=='B') return this.B[this.B.length-count];
		else if(stack=='C') return this.C[this.C.length-count];
		else if(stack=='D') return this.D[this.D.length-count];
		else if(stack=='E') return this.E[this.E.length-count];
		else if(stack=='F') return this.F[this.F.length-count];
		else return 0;
	}

	public void addRemove(int stack1, int stack2){
		this.addCard(stack1,this.getCard(stack1));
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, int value, int stack2){
		this.addCard(stack1,value);
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, boolean pass1, int stack2){
		this.addCard(stack1,this.getCard(stack1),pass1);
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, int value, boolean pass1, int stack2){
		this.addCard(stack1,value,pass1);
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, int stack2, boolean pass2){
		this.addCard(stack1,this.getCard(stack1));
		this.removeCard(stack2,pass2);
	}

	public void addRemove(int stack1, int value,int stack2, boolean pass2){
		this.addCard(stack1,value);
		this.removeCard(stack2,pass2);
	}

	public void addRemove(int stack1, boolean pass1, int stack2, boolean pass2){
		this.addCard(stack1,this.getCard(stack1),pass1);
		this.removeCard(stack2,pass2);
	}

	public void addRemove(int stack1, int value, boolean pass1, int stack2, boolean pass2){
		this.addCard(stack1,value,pass1);
		this.removeCard(stack2,pass2);
	}

	public void addRemove(char stack1, int stack2){
		this.addCard(stack1,this.getCard(stack1));
		this.removeCard(stack2);
	}

	public void addRemove(char stack1, int value, int stack2){
		this.addCard(stack1,value);
		this.removeCard(stack2);
	}

	public void addRemove(char stack1, int stack2, boolean pass2){
		this.addCard(stack1,this.getCard(stack1));
		this.removeCard(stack2,pass2);
	}

	public void addRemove(char stack1, int value, int stack2, boolean pass2){
		this.addCard(stack1,value);
		this.removeCard(stack2,pass2);
	}

	public void addRemove(int stack1, char stack2){
		this.addCard(stack1,this.getCard(stack1));
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, int value, char stack2){
		this.addCard(stack1,value);
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, boolean pass1, char stack2){
		this.addCard(stack1,this.getCard(stack1),pass1);
		this.removeCard(stack2);
	}

	public void addRemove(int stack1, int value, boolean pass1, char stack2){
		this.addCard(stack1,value,pass1);
		this.removeCard(stack2);
	}

	public String card(int cardNum){
		String card = new String();
		
		if((cardNum-1)/13==0) card += "S";
		else if((cardNum-1)/13==1) card += "H";
		else if((cardNum-1)/13==2) card += "D";
		else if((cardNum-1)/13==3) card += "C";

		if(cardNum%13==0) card += "K";
		else if(cardNum%13<=10) card += Integer.toString(cardNum%13);
		else if(cardNum%13==11) card += "J";
		else if(cardNum%13==12) card += "Q";
		return card;
	}
}