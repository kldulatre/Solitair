/*
KENJIE LLOYD DULATRE
2014-28334
CS12 MP2 SOLITAIR WITH UI
under PROF. MARIO CARREON
MAY 24, 2016
*/

import java.util.*;
import java.io.*;

class Node{
	public int card;
	public boolean isOpen;
	public Node next;

	public Node(int card,boolean isOpen){
		this.card = card;
		this.isOpen = isOpen;
	}	

	public String getCard(int cardNum){
		String card = new String();
		
		if((cardNum-1)/13==0){
			card += "SB";
		}
		else if((cardNum-1)/13==1){
			card += "HR";
		}
		else if((cardNum-1)/13==2){
			card += "DR";
		}
		else if((cardNum-1)/13==3){
			card += "CB";
		}

		if(cardNum%13==0){
			card += "K";
		}
		else if(cardNum%13<=10){
			card += Integer.toString(cardNum%13);
		}
		else if(cardNum%13==11){
			card += "J";
		}
		else if(cardNum%13==12){
			card += "Q";
		}
		
		if(cardNum%13!=10){
			card = " " + card;
		}

		return card;
	}
}

public class Stack{
	public Node head;
	
	public Stack (){
		head = null;
	}

	public boolean isEmpty(){
		return (head == null); // returns true
	}

	public void insertNode(int card){
		Node newNode = new Node(card,false);
		newNode.next = head;
		head = newNode;
	}

	public void insertNode(int card, boolean isOpen){
		Node newNode = new Node(card,isOpen);
		newNode.next = head;
		head = newNode;
	}

	public Boolean find(int card){
		Node tempNode = head;
		if(!this.isEmpty()){
			while(tempNode.card != card){
				if(tempNode.next == null){
					return false;
				}
				else{
					tempNode = tempNode.next;
				}
			}
		}
		else{
			return false;
		}
		return true;
	}

	public Boolean remove(int card){
		Node tempNode = head;
		Node prevNode = head;
 		if(!this.isEmpty()){
			while(tempNode.card != card){
				if(tempNode.next == null){
					return false;
				}
				else{
					prevNode = tempNode;
					tempNode = tempNode.next;
				}
			}
		}
		else{
			return false;
		}

		if(tempNode==head){
			head = head.next;
		}
		else{
			prevNode.next = tempNode.next;
		}
		return true;
	}

	private int getLength(boolean open){
		Node tempNode = head;
		int counter = 0;
		if(!open){
			while(tempNode!=null){
				counter++;
				tempNode = tempNode.next;
			}
		}
		else{
			while(tempNode.isOpen){
				counter++;
				tempNode = tempNode.next;
			}
		}
		return counter;
	}

	public int getCards(int counter){
		int limit = this.getLength(false);
		int holder = 0;
		Node tempNode = head;

		if(counter>limit){
			return 0;
		}
		else{
			for(int x=0;counter>x;x++){
				holder = tempNode.card;
				tempNode = tempNode.next;
			}
			return holder;
		}
	}
			

	public int getTop(){
		if(head!=null)
			return head.card;
		else
			return 0;
	}

	public void openTop(){
		if(head!=null&&head.isOpen==false){
			head.isOpen = true;
		}
	}

	public Boolean removeTop(){
		Node tempNode = head;
		if(!this.isEmpty()){
			head = head.next;
		}
		else{
			return false;
		}
		return true;
	}

	public int[] getOpenCards(){
		int[] tempArray = new int[0];
		Node temp = head;
		while(temp!=null&&temp.isOpen!=false){
			tempArray = Arrays.copyOf(tempArray,tempArray.length+1);
			tempArray[tempArray.length-1] = temp.card;
			temp = temp.next;
		}
		return tempArray;
	}

	public int[] getCardsArray(){
		int[] tempArray = new int[0];
		Node temp = head;
		while(temp!=null){
			tempArray = Arrays.copyOf(tempArray,tempArray.length+1);
			if(temp.isOpen==false)
				tempArray[tempArray.length-1] = 0;
			else
				tempArray[tempArray.length-1] = temp.card;
			temp = temp.next;
		}
		return tempArray;
	}

	public Boolean isFinish(){
		Node temp = head;
		while(temp!=null&&temp.isOpen==true){
			temp = temp.next;
		}
		if(temp==null){
			return true;
		}
		else{
			return false;
		}
	}

}