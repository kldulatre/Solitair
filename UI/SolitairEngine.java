/*
KENJIE LLOYD DULATRE
2014-28334
CS12 MP2 SOLITAIR WITH UI
under PROF. MARIO CARREON
MAY 24, 2016
*/

import java.util.*;
import java.io.*;

public class SolitairEngine{
	Stack stack1,stack2,stack3,stack4,stack5,stack6,stack7;
	Stack suite1,suite2,suite3,suite4;
	Stack deck1,deck2;
	private int[] tempDeck;

	public SolitairEngine(){
		this.newGame();
	}

	public void newGame(){
		this.tempDeck = new int[0];
		for(int x=1;x<=52;x++){
			this.tempDeck = Arrays.copyOf(this.tempDeck,this.tempDeck.length+1);
			this.tempDeck[this.tempDeck.length-1] = x;
		}
		stack1 = new Stack();
		stack2 = new Stack();
		stack3 = new Stack();
		stack4 = new Stack();
		stack5 = new Stack();
		stack6 = new Stack();
		stack7 = new Stack();
		suite1 = new Stack();
		suite2 = new Stack();
		suite3 = new Stack();
		suite4 = new Stack();
		deck1 = new Stack();
		deck2 = new Stack();
		this.shuffleCards();
		this.distributeCards();
	}

	private void shuffleCards(){
		Random random = new Random();
		for(int x=0;x<52;x++){
			this.swap(random.nextInt(52),random.nextInt(52));
		}
	}

	private void swap(int x, int y){
		Integer temp = tempDeck[x];
		tempDeck[x] = tempDeck[y];
		tempDeck[y] = temp;
	}

	private void distributeCards(){
		for(int x=0;x<52;x++){
			if(x<1) this.stack1.insertNode(this.tempDeck[x]);
			else if(x<3) this.stack2.insertNode(this.tempDeck[x]);
			else if(x<6) this.stack3.insertNode(this.tempDeck[x]);
			else if(x<10) this.stack4.insertNode(this.tempDeck[x]);
			else if(x<15) this.stack5.insertNode(this.tempDeck[x]);
			else if(x<21) this.stack6.insertNode(this.tempDeck[x]);
			else if(x<28) this.stack7.insertNode(this.tempDeck[x]);
			else this.deck1.insertNode(this.tempDeck[x],true);
		}
	}

	public void exchangeDeck(){
		if(deck1.getTop()!=0){
			deck2.insertNode(deck1.getTop(),true);
			deck1.removeTop();
		}
		else{
			while(deck2.getTop()!=0){
				deck1.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
	}

	public void putToSuite(int answer2){
		if(answer2==11){
			if((suite1.isEmpty())&&(deck2.getTop()==1||deck2.getTop()==14||deck2.getTop()==27||deck2.getTop()==40)){
				suite1.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
			else if(checkSuite(suite1.getTop(),deck2.getTop())){
				suite1.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2==22){
			if((suite2.isEmpty())&&(deck2.getTop()==1||deck2.getTop()==14||deck2.getTop()==27||deck2.getTop()==40)){
				suite2.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
			else if(checkSuite(suite2.getTop(),deck2.getTop())){
				suite2.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2==33){
			if((suite3.isEmpty())&&(deck2.getTop()==1||deck2.getTop()==14||deck2.getTop()==27||deck2.getTop()==40)){
				suite3.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
			else if(checkSuite(suite3.getTop(),deck2.getTop())){
				suite3.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2==44){
			if((suite4.isEmpty())&&(deck2.getTop()==1||deck2.getTop()==14||deck2.getTop()==27||deck2.getTop()==40)){
				suite4.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
			else if(checkSuite(suite4.getTop(),deck2.getTop())){
				suite4.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
	}

	public void putToStack(int answer2){
		if(answer2 == 1){
			if(stack1.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack1.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack1.getTop(),deck2.getTop())){
				stack1.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 2){
			if(stack2.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack2.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack2.getTop(),deck2.getTop())){
				stack2.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 3){
			if(stack3.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack3.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack3.getTop(),deck2.getTop())){
				stack3.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 4){
			if(stack4.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack4.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack4.getTop(),deck2.getTop())){
				stack4.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 5){
			if(stack5.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack5.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack5.getTop(),deck2.getTop())){
				stack5.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 6){
			if(stack6.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack6.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack6.getTop(),deck2.getTop())){
				stack6.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
		else if(answer2 == 7){
			if(stack7.isEmpty()){
				if(deck2.getTop()==13||deck2.getTop()==26||deck2.getTop()==39||deck2.getTop()==52){
					stack7.insertNode(deck2.getTop(),true);
					deck2.removeTop();
				}
			}
			else if(checker(stack7.getTop(),deck2.getTop())){
				stack7.insertNode(deck2.getTop(),true);
				deck2.removeTop();
			}
		}
	}

	public void suiteToStack1(int answer2){
		if(answer2 == 1){
			if(stack1.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack1.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack1.getTop(),suite1.getTop())){
				stack1.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
		else if(answer2 == 2){
			if(stack2.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack2.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack2.getTop(),suite1.getTop())){
				stack2.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}	
		else if(answer2 == 3){
			if(stack3.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack3.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack3.getTop(),suite1.getTop())){
				stack3.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
		else if(answer2 == 4){
			if(stack4.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack4.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack4.getTop(),suite1.getTop())){
				stack4.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
		else if(answer2 == 5){
			if(stack5.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack5.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack5.getTop(),suite1.getTop())){
				stack5.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
		else if(answer2 == 6){
			if(stack6.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack6.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack6.getTop(),suite1.getTop())){
				stack6.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
		else if(answer2 == 7){
			if(stack7.isEmpty()){
				if(suite1.getTop()==13||suite1.getTop()==26||suite1.getTop()==39||suite1.getTop()==52){
					stack7.insertNode(suite1.getTop(),true);
					suite1.removeTop();
				}
			}
			else if(checker(stack7.getTop(),suite1.getTop())){
				stack7.insertNode(suite1.getTop(),true);
				suite1.removeTop();
			}
		}
	}

	public void suiteToStack2(int answer2){
		if(answer2 == 1){
			if(stack1.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack1.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack1.getTop(),suite2.getTop())){
				stack1.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
		else if(answer2 == 2){
			if(stack2.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack2.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack2.getTop(),suite2.getTop())){
				stack2.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}	
		else if(answer2 == 3){
			if(stack3.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack3.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack3.getTop(),suite2.getTop())){
				stack3.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
		else if(answer2 == 4){
			if(stack4.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack4.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack4.getTop(),suite2.getTop())){
				stack4.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
		else if(answer2 == 5){
			if(stack5.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack5.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack5.getTop(),suite2.getTop())){
				stack5.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
		else if(answer2 == 6){
			if(stack6.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack6.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack6.getTop(),suite2.getTop())){
				stack6.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
		else if(answer2 == 7){
			if(stack7.isEmpty()){
				if(suite2.getTop()==13||suite2.getTop()==26||suite2.getTop()==39||suite2.getTop()==52){
					stack7.insertNode(suite2.getTop(),true);
					suite2.removeTop();
				}
			}
			else if(checker(stack7.getTop(),suite2.getTop())){
				stack7.insertNode(suite2.getTop(),true);
				suite2.removeTop();
			}
		}
	}

	public void suiteToStack3(int answer2){
		if(answer2 == 1){
			if(stack1.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack1.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack1.getTop(),suite3.getTop())){
				stack1.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
		else if(answer2 == 2){
			if(stack2.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack2.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack2.getTop(),suite3.getTop())){
				stack2.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}	
		else if(answer2 == 3){
			if(stack3.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack3.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack3.getTop(),suite3.getTop())){
				stack3.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
		else if(answer2 == 4){
			if(stack4.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack4.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack4.getTop(),suite3.getTop())){
				stack4.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
		else if(answer2 == 5){
			if(stack5.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack5.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack5.getTop(),suite3.getTop())){
				stack5.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
		else if(answer2 == 6){
			if(stack6.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack6.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack6.getTop(),suite3.getTop())){
				stack6.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
		else if(answer2 == 7){
			if(stack7.isEmpty()){
				if(suite3.getTop()==13||suite3.getTop()==26||suite3.getTop()==39||suite3.getTop()==52){
					stack7.insertNode(suite3.getTop(),true);
					suite3.removeTop();
				}
			}
			else if(checker(stack7.getTop(),suite3.getTop())){
				stack7.insertNode(suite3.getTop(),true);
				suite3.removeTop();
			}
		}
	}

	public void suiteToStack4(int answer2){
		if(answer2 == 1){
			if(stack1.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack1.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack1.getTop(),suite4.getTop())){
				stack1.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
		else if(answer2 == 2){
			if(stack2.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack2.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack2.getTop(),suite4.getTop())){
				stack2.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}	
		else if(answer2 == 3){
			if(stack3.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack3.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack3.getTop(),suite4.getTop())){
				stack3.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
		else if(answer2 == 4){
			if(stack4.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack4.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack4.getTop(),suite4.getTop())){
				stack4.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
		else if(answer2 == 5){
			if(stack5.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack5.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack5.getTop(),suite4.getTop())){
				stack5.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
		else if(answer2 == 6){
			if(stack6.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack6.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack6.getTop(),suite4.getTop())){
				stack6.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
		else if(answer2 == 7){
			if(stack7.isEmpty()){
				if(suite4.getTop()==13||suite4.getTop()==26||suite4.getTop()==39||suite4.getTop()==52){
					stack7.insertNode(suite4.getTop(),true);
					suite4.removeTop();
				}
			}
			else if(checker(stack7.getTop(),suite4.getTop())){
				stack7.insertNode(suite4.getTop(),true);
				suite4.removeTop();
			}
		}
	}

	public void stackToSuite1(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack1.getTop()==1||stack1.getTop()==14||stack1.getTop()==27||stack1.getTop()==40)){
				suite1.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack1.getTop())){
				suite1.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack1.getTop()==1||stack1.getTop()==14||stack1.getTop()==27||stack1.getTop()==40)){
				suite2.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack1.getTop())){
				suite2.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack1.getTop()==1||stack1.getTop()==14||stack1.getTop()==27||stack1.getTop()==40)){
				suite3.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack1.getTop())){
				suite3.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack1.getTop()==1||stack1.getTop()==14||stack1.getTop()==27||stack1.getTop()==40)){
				suite4.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack1.getTop())){
				suite4.insertNode(stack1.getTop(),true);
				stack1.removeTop();
			}	
		}
	}

	public void stackToSuite2(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack2.getTop()==1||stack2.getTop()==14||stack2.getTop()==27||stack2.getTop()==40)){
				suite1.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack2.getTop())){
				suite1.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack2.getTop()==1||stack2.getTop()==14||stack2.getTop()==27||stack2.getTop()==40)){
				suite2.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack2.getTop())){
				suite2.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack2.getTop()==1||stack2.getTop()==14||stack2.getTop()==27||stack2.getTop()==40)){
				suite3.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack2.getTop())){
				suite3.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack2.getTop()==1||stack2.getTop()==14||stack2.getTop()==27||stack2.getTop()==40)){
				suite4.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack2.getTop())){
				suite4.insertNode(stack2.getTop(),true);
				stack2.removeTop();
			}	
		}
	}

	public void stackToSuite3(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack3.getTop()==1||stack3.getTop()==14||stack3.getTop()==27||stack3.getTop()==40)){
				suite1.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack3.getTop())){
				suite1.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack3.getTop()==1||stack3.getTop()==14||stack3.getTop()==27||stack3.getTop()==40)){
				suite2.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack3.getTop())){
				suite2.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack3.getTop()==1||stack3.getTop()==14||stack3.getTop()==27||stack3.getTop()==40)){
				suite3.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack3.getTop())){
				suite3.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack3.getTop()==1||stack3.getTop()==14||stack3.getTop()==27||stack3.getTop()==40)){
				suite4.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack3.getTop())){
				suite4.insertNode(stack3.getTop(),true);
				stack3.removeTop();
			}	
		}
	}

	public void stackToSuite4(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack4.getTop()==1||stack4.getTop()==14||stack4.getTop()==27||stack4.getTop()==40)){
				suite1.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack4.getTop())){
				suite1.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack4.getTop()==1||stack4.getTop()==14||stack4.getTop()==27||stack4.getTop()==40)){
				suite2.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack4.getTop())){
				suite2.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack4.getTop()==1||stack4.getTop()==14||stack4.getTop()==27||stack4.getTop()==40)){
				suite3.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack4.getTop())){
				suite3.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack4.getTop()==1||stack4.getTop()==14||stack4.getTop()==27||stack4.getTop()==40)){
				suite4.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack4.getTop())){
				suite4.insertNode(stack4.getTop(),true);
				stack4.removeTop();
			}	
		}
	}

	public void stackToSuite5(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack5.getTop()==1||stack5.getTop()==14||stack5.getTop()==27||stack5.getTop()==40)){
				suite1.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack5.getTop())){
				suite1.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack5.getTop()==1||stack5.getTop()==14||stack5.getTop()==27||stack5.getTop()==40)){
				suite2.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack5.getTop())){
				suite2.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack5.getTop()==1||stack5.getTop()==14||stack5.getTop()==27||stack5.getTop()==40)){
				suite3.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack5.getTop())){
				suite3.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack5.getTop()==1||stack5.getTop()==14||stack5.getTop()==27||stack5.getTop()==40)){
				suite4.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack5.getTop())){
				suite4.insertNode(stack5.getTop(),true);
				stack5.removeTop();
			}	
		}
	}

	public void stackToSuite6(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack6.getTop()==1||stack6.getTop()==14||stack6.getTop()==27||stack6.getTop()==40)){
				suite1.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack6.getTop())){
				suite1.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack6.getTop()==1||stack6.getTop()==14||stack6.getTop()==27||stack6.getTop()==40)){
				suite2.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack6.getTop())){
				suite2.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack6.getTop()==1||stack6.getTop()==14||stack6.getTop()==27||stack6.getTop()==40)){
				suite3.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack6.getTop())){
				suite3.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack6.getTop()==1||stack6.getTop()==14||stack6.getTop()==27||stack6.getTop()==40)){
				suite4.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack6.getTop())){
				suite4.insertNode(stack6.getTop(),true);
				stack6.removeTop();
			}	
		}
	}

	public void stackToSuite7(int answer2){
		if(answer2 == 11){
			if((suite1.isEmpty())&&(stack7.getTop()==1||stack7.getTop()==14||stack7.getTop()==27||stack7.getTop()==40)){
				suite1.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}
			else if(checkSuite(suite1.getTop(),stack7.getTop())){
				suite1.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}
		}
		else if(answer2 == 22){
			if((suite2.isEmpty())&&(stack7.getTop()==1||stack7.getTop()==14||stack7.getTop()==27||stack7.getTop()==40)){
				suite2.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}
			else if(checkSuite(suite2.getTop(),stack7.getTop())){
				suite2.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}	
		}
		else if(answer2 == 33){
			if((suite3.isEmpty())&&(stack7.getTop()==1||stack7.getTop()==14||stack7.getTop()==27||stack7.getTop()==40)){
				suite3.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}
			else if(checkSuite(suite3.getTop(),stack7.getTop())){
				suite3.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}	
		}
		else if(answer2 == 44){
			if((suite4.isEmpty())&&(stack7.getTop()==1||stack7.getTop()==14||stack7.getTop()==27||stack7.getTop()==40)){
				suite4.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}
			else if(checkSuite(suite4.getTop(),stack7.getTop())){
				suite4.insertNode(stack7.getTop(),true);
				stack7.removeTop();
			}	
		}
	}

	public void stackToStack1(int answer2){
		if(answer2 == 2){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
		else if(answer2 == 6){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack1.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack1)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack1.removeTop();
				}	
			}
		}
	}

	public void stackToStack2(int answer2){
		if(answer2 == 1){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
		else if(answer2 == 6){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack2.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack2)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack2.removeTop();
				}	
			}
		}
	}

	public void stackToStack3(int answer2){
		if(answer2 == 1){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
		else if(answer2 == 2){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
		else if(answer2 == 6){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack3.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack3)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack3.removeTop();
				}	
			}
		}
	}

	public void stackToStack4(int answer2){
		if(answer2 == 1){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
		else if(answer2 == 2){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
		else if(answer2 == 6){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack4.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack4)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack4.removeTop();
				}	
			}
		}
	}

	public void stackToStack5(int answer2){
		if(answer2 == 1){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
		}
		else if(answer2 == 2){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}	
		}
		else if(answer2 == 6){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack5.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack5)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack5.removeTop();
				}	
			}
		}
	}

	public void stackToStack6(int answer2){
		if(answer2 == 1){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
		else if(answer2 == 2){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
		else if(answer2 == 7){
			int[] newArray = stack6.getOpenCards();
			int y = stackToStackChecker(stack7.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack6)){
				for(int x=y-1;x>=0;x--){
					stack7.insertNode(newArray[x],true);
					stack6.removeTop();
				}	
			}
		}
	}

	public void stackToStack7(int answer2){
		if(answer2 == 1){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack1.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack1.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
		else if(answer2 == 2){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack2.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack2.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
		else if(answer2 == 3){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack3.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack3.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
		else if(answer2 == 4){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack4.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack4.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
		else if(answer2 == 5){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack5.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack5.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
		else if(answer2 == 6){
			int[] newArray = stack7.getOpenCards();
			int y = stackToStackChecker(stack6.getTop(),newArray);
			if(y!=-1){
				for(int x=y;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
			y = newArray.length;
			if(stackKingChecker(stack7)){
				for(int x=y-1;x>=0;x--){
					stack6.insertNode(newArray[x],true);
					stack7.removeTop();
				}	
			}
		}
	}

	private Boolean stackKingChecker(Stack stack){
		int[] openCards = stack.getOpenCards();
		int openLength = openCards.length;
		for(int x=0;x<openLength;x++){
			if(openCards[x]==13||openCards[x]==26||openCards[x]==39||openCards[x]==52){
				return true;
			}
		}
		return false;
	}

	private Boolean checkColor(int card1,int card2){
		int pass1=0,pass2=0; 
		
		if((1<=card1 && card1<=13) || (40<=card1 && card1<=52)){
			pass1 = 1;
		}
		else{
			pass1 = 2;
		}

		if((1<=card2 && card2<=13) || (40<=card2 && card2<=52)){
			pass2 = 1;
		}
		else{
			pass2 = 2;
		}

		if(pass1!=pass2){
			return true;
		}
		else{
			return false;
		}
	}

	private Boolean checkNum(int card1,int card2){
		if( (card2-1)%13 >= (card1-1)%13 ) {
			return false;
		}
		else {
			if((card1-1)%13-1==(card2-1)%13){
				return true;
			}
			else{
				return false;
			}
		}
	}

	private Boolean checker(int card1,int card2){ //card1 ay mas malaki kay card2
		Boolean pass1 = checkColor(card1,card2);
		Boolean pass2 = checkNum(card1,card2);
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}

	private int stackToStackChecker(int myStack1, int[] myStack2){
		for(int x = 0;x<myStack2.length;x++){
			boolean pass = checker(myStack1,myStack2[x]);
			if(pass) 
				return x;
		}
		return -1;
	}

	private Boolean checkSuiteType(int card1,int card2){
		if((card1-1)/13==(card2-1)/13){
			return true;
		}	
		else{
			return false;
		}
	}

	private Boolean checkSuiteNum(int card1,int card2){
		if( (card2-1)%13 < (card1-1)%13 ) {
			return false;
		}
		else {
			if((card1-1)%13==(card2-1)%13-1){
				return true;
			}
			else{
				return false;
			}
		}
	}

	private Boolean checkSuite(int card1,int card2){
		Boolean pass1 = checkSuiteType(card1,card2);
		Boolean pass2 = checkSuiteNum(card1,card2);
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}

	public Boolean isWinner(){
		Boolean s1 = stack1.isFinish();
		Boolean s2 = stack2.isFinish();
		Boolean s3 = stack3.isFinish();
		Boolean s4 = stack4.isFinish();
		Boolean s5 = stack5.isFinish();
		Boolean s6 = stack6.isFinish();
		Boolean s7 = stack7.isFinish();
		if(s1 && s2 && s3 && s4 && s5 && s6 && s7){
			return true;
		}
		else{
			return false;
		}
	}

	public void cardOpener(){
		stack1.openTop();
		stack2.openTop();
		stack3.openTop();
		stack4.openTop();
		stack5.openTop();
		stack6.openTop();
		stack7.openTop();
	}
}