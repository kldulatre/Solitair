/*
KENJIE LLOYD DULATRE
2014-28334
CS12 MP2 SOLITAIR WITH UI
under PROF. MARIO CARREON
MAY 24, 2016
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;

public class UI extends SolitairEngine implements ActionListener{
	JFrame masterFrame;
	JPanel rightPanel,leftPanel;
	JButton[] buttonArray1,buttonArray2,buttonArray3,buttonArray4,buttonArray5,buttonArray6,buttonArray7;
	JPanel northPanel,centerPanel,southPanel;
	JPanel stackPanel1,stackPanel2,stackPanel3,stackPanel4,stackPanel5,stackPanel6,stackPanel7;
	JButton suiteButton1,suiteButton2,suiteButton3,suiteButton4;
	JButton dummyButton,deckButton1,deckButton2;
	JButton stackButton1,stackButton2,stackButton3,stackButton4,stackButton5,stackButton6,stackButton7;
	JButton newGameButton,checkGameButton,quitGameButton;

	Boolean pass = false;
	int answer1 = 0;
	int answer2 = 0;

	public UI(){
		masterFrame = new JFrame("Solitair CS12 MP2 kldulatre");
		masterFrame.setSize(925,630);
		masterFrame.setLayout(new FlowLayout(FlowLayout.LEFT));
		masterFrame.setLocationRelativeTo(null);

		cardOpener();

		leftPanel = new JPanel();
		this.addLeftButtons();
		
		northPanel = new JPanel();
		this.addNorthButtons();

		centerPanel = new JPanel();
		this.addCenterButtons();

		southPanel = new JPanel();
		this.addSouthButtons();

		rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());
		rightPanel.add(northPanel,BorderLayout.NORTH);
		rightPanel.add(centerPanel,BorderLayout.CENTER);
		rightPanel.add(southPanel,BorderLayout.SOUTH);

		masterFrame.add(leftPanel);
		masterFrame.add(rightPanel);
		masterFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		masterFrame.setVisible(true);

		this.answer1=99;
		this.answer2=99;
		this.update();
	}

	public void addLeftButtons(){
		newGameButton = new JButton("New Game");
		newGameButton.setIgnoreRepaint(true);
        newGameButton.setFocusable(false);
        newGameButton.setContentAreaFilled(false);
        newGameButton.setPreferredSize(new Dimension(120,25));
        newGameButton.addActionListener(this);

        checkGameButton = new JButton("Check Game");
		checkGameButton.setIgnoreRepaint(true);
        checkGameButton.setFocusable(false);
        checkGameButton.setContentAreaFilled(false);
        checkGameButton.setPreferredSize(new Dimension(120,25));
        checkGameButton.addActionListener(this);

        quitGameButton = new JButton("Quit Game");
		quitGameButton.setIgnoreRepaint(true);
        quitGameButton.setFocusable(false);
        quitGameButton.setContentAreaFilled(false);
        quitGameButton.setPreferredSize(new Dimension(120,25));
        quitGameButton.addActionListener(this);

		leftPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		leftPanel.setPreferredSize(new Dimension(150,580));
		leftPanel.add(newGameButton);
		leftPanel.add(Box.createRigidArea(new Dimension(0,20)));
		leftPanel.add(checkGameButton);
		leftPanel.add(Box.createRigidArea(new Dimension(0,40)));
		leftPanel.add(quitGameButton);
		leftPanel.add(Box.createRigidArea(new Dimension(0,20)));
	}

	private void addButtonDecoration(JButton[] tempArray,Boolean pass){
		for(int x=0;x<=6;x++){
			if(x==4&&pass==true){
				tempArray[x].setBorder(null);
			}
			tempArray[x].setIgnoreRepaint(true);
	        tempArray[x].setFocusable(false);
	        tempArray[x].setContentAreaFilled(false);
	        tempArray[x].setMaximumSize(new Dimension(100, 25));
			tempArray[x].setPreferredSize(new Dimension(100,25));
		}
	}

	public void addNorthButtons(){
		suiteButton1 = new JButton();
		suiteButton2 = new JButton();
		suiteButton3 = new JButton();
		suiteButton4 = new JButton();
		dummyButton = new JButton();
		deckButton1 = new JButton();
		deckButton2 = new JButton();
		
		JButton[] tempArray = {suiteButton1,suiteButton2,suiteButton3,suiteButton4,dummyButton,deckButton1,deckButton2};
		addButtonDecoration(tempArray,true);

		suiteButton1.addActionListener(this);
		suiteButton2.addActionListener(this);
		suiteButton3.addActionListener(this);
		suiteButton4.addActionListener(this);
		deckButton1.addActionListener(this);
		deckButton2.addActionListener(this);

		northPanel.add(suiteButton1);
		northPanel.add(suiteButton2);
		northPanel.add(suiteButton3);
		northPanel.add(suiteButton4);
		northPanel.add(dummyButton);
		northPanel.add(deckButton2);
		northPanel.add(deckButton1);
	}

	public void addCenterButtons(){
		stackButton1 = new JButton("1");
		stackButton2 = new JButton("2");
		stackButton3 = new JButton("3");
		stackButton4 = new JButton("4");
		stackButton5 = new JButton("5");
		stackButton6 = new JButton("6");
		stackButton7 = new JButton("7");

		JButton[] tempArray = {stackButton1,stackButton2,stackButton3,stackButton4,stackButton5,stackButton6,stackButton7};
		addButtonDecoration(tempArray,false);

		stackButton1.addActionListener(this);
		stackButton2.addActionListener(this);
		stackButton3.addActionListener(this);
		stackButton4.addActionListener(this);
		stackButton5.addActionListener(this);
		stackButton6.addActionListener(this);
		stackButton7.addActionListener(this);

		centerPanel.add(stackButton1);
		centerPanel.add(stackButton2);
		centerPanel.add(stackButton3);
		centerPanel.add(stackButton4);
		centerPanel.add(stackButton5);
		centerPanel.add(stackButton6);
		centerPanel.add(stackButton7);
	}

	private void addSouthPanelButtons(JPanel panel){
		if(panel == stackPanel1) buttonArray1 = new JButton[18];
		else if(panel == stackPanel2) buttonArray2 = new JButton[18];
		else if(panel == stackPanel3) buttonArray3 = new JButton[18];
		else if(panel == stackPanel4) buttonArray4 = new JButton[18];
		else if(panel == stackPanel5) buttonArray5 = new JButton[18];
		else if(panel == stackPanel6) buttonArray6 = new JButton[18];
		else if(panel == stackPanel7) buttonArray7 = new JButton[18]; 
		for(int x=0;x<=17;x++){
			JButton button = new JButton();
			button.setIgnoreRepaint(true);
	        button.setFocusable(false);
	        button.setContentAreaFilled(false);
			button.setPreferredSize(new Dimension(100,25));
			button.setEnabled(false);
			button.setBorder(null);
			panel.add(button);
			if(panel == stackPanel1) buttonArray1[x] = button;
			else if(panel == stackPanel2) buttonArray2[x] = button;
			else if(panel == stackPanel3) buttonArray3[x] = button;
			else if(panel == stackPanel4) buttonArray4[x] = button;
			else if(panel == stackPanel5) buttonArray5[x] = button;
			else if(panel == stackPanel6) buttonArray6[x] = button;
			else if(panel == stackPanel7) buttonArray7[x] = button;
		}
	}

	public void addSouthButtons(){
		stackPanel1 = new JPanel();
		stackPanel2 = new JPanel();
		stackPanel3 = new JPanel();
		stackPanel4 = new JPanel();
		stackPanel5 = new JPanel();
		stackPanel6 = new JPanel();
		stackPanel7 = new JPanel();

		stackPanel1.setPreferredSize(new Dimension(100,500));
		stackPanel2.setPreferredSize(new Dimension(100,500));
		stackPanel3.setPreferredSize(new Dimension(100,500));
		stackPanel4.setPreferredSize(new Dimension(100,500));
		stackPanel5.setPreferredSize(new Dimension(100,500));
		stackPanel6.setPreferredSize(new Dimension(100,500));
		stackPanel7.setPreferredSize(new Dimension(100,500));

		stackPanel1.setLayout(new GridLayout(18,1));
		stackPanel2.setLayout(new GridLayout(18,1));
		stackPanel3.setLayout(new GridLayout(18,1));
		stackPanel4.setLayout(new GridLayout(18,1));
		stackPanel5.setLayout(new GridLayout(18,1));
		stackPanel6.setLayout(new GridLayout(18,1));
		stackPanel7.setLayout(new GridLayout(18,1));

		this.addSouthPanelButtons(stackPanel1);
		this.addSouthPanelButtons(stackPanel2);
		this.addSouthPanelButtons(stackPanel3);
		this.addSouthPanelButtons(stackPanel4);
		this.addSouthPanelButtons(stackPanel5);
		this.addSouthPanelButtons(stackPanel6);
		this.addSouthPanelButtons(stackPanel7);

		southPanel.add(stackPanel1);
		southPanel.add(stackPanel2);
		southPanel.add(stackPanel3);
		southPanel.add(stackPanel4);
		southPanel.add(stackPanel5);
		southPanel.add(stackPanel6);
		southPanel.add(stackPanel7);
	}

	private String getCard(int cardNum){
		String card = new String();
		
		if((cardNum-1)/13==0){
			card += "S";
		}
		else if((cardNum-1)/13==1){
			card += "H";
		}
		else if((cardNum-1)/13==2){
			card += "D";
		}
		else if((cardNum-1)/13==3){
			card += "C";
		}

		if(cardNum%13==0){
			card += "K";
		}
		else if(cardNum%13<=10){
			card += Integer.toString(cardNum%13);
		}
		else if(cardNum%13==11){
			card += "J";
		}
		else if(cardNum%13==12){
			card += "Q";
		}
		
		if(cardNum%13!=10){
			card = " " + card;
		}

		return card;
	}

	private Color getBackground(int cardNum){
		Color color;
		if(cardNum==0){
			color = new Color(34,238,91);
		}
		else if(cardNum<=13||cardNum>=40){
			color = new Color(254,57,57);
		}
		else{
			color = new Color(51,51,51);
		}
		return color;
	}

	private Color getForeground(int cardNum){
		Color color;
		if(cardNum==0){
			color = Color.BLACK;
		}
		else if(cardNum<=13||cardNum>=40){
			color = Color.BLACK;
		}
		else{
			color = Color.WHITE;
		}
		return color;	
	}

	private void updateView(){
		if(this.answer1==0&&this.answer2==0){
			this.updateTextDeck1();
			this.updateTextDeck2();
		}
		else if(this.answer1==9&&this.answer2==9){
			this.updateTextDeck2();
		}
		else if(this.answer1==1&&this.answer2==1){
			this.updateTextStack1();
		}
		else if(this.answer1==2&&this.answer2==2){
			this.updateTextStack2();
		}
		else if(this.answer1==3&&this.answer2==3){
			this.updateTextStack3();
		}
		else if(this.answer1==4&&this.answer2==4){
			this.updateTextStack4();
		}
		else if(this.answer1==5&&this.answer2==5){
			this.updateTextStack5();
		}
		else if(this.answer1==6&&this.answer2==6){
			this.updateTextStack6();
		}
		else if(this.answer1==7&&this.answer2==7){
			this.updateTextStack1();
		}
		else if(this.answer1==11&&this.answer2==11){
			this.updateTextSuite1();
		}
		else if(this.answer1==22&&this.answer2==22){
			this.updateTextSuite2();
		}
		else if(this.answer1==33&&this.answer2==33){
			this.updateTextSuite3();
		}
		else if(this.answer1==44&&this.answer2==44){
			this.updateTextSuite4();
		}
		else{
			this.updateTextDeck1();
			this.updateTextDeck2();
			this.updateTextStack1();
			this.updateTextStack2();
			this.updateTextStack3();
			this.updateTextStack4();
			this.updateTextStack5();
			this.updateTextStack6();
			this.updateTextStack7();
			this.updateTextSuite1();
			this.updateTextSuite2();
			this.updateTextSuite3();
			this.updateTextSuite4();
		}
	}

	private void updateButton(JButton button,int cardNum){
		if(cardNum!=0)
			button.setText(getCard(cardNum));
		else
			button.setText("");
		button.setBackground(this.getBackground(cardNum));
		button.setForeground(this.getForeground(cardNum));
		button.setContentAreaFilled(true);
		button.setEnabled(true);
		button.setBorder(new EtchedBorder());
		button.repaint();
	}

	private void resetButton(JButton button,Boolean pass){
		button.setText("");	
		button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setContentAreaFilled(false);
		button.setPreferredSize(new Dimension(100,25));
		// button.setEnabled(false);
		if(pass)
			button.setBorder(null);
	}

	private void updateTextStack1(){
		int[] stackArray = stack1.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray1[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray1[x],true);
		}	
	}

	private void updateTextStack2(){
		int[] stackArray = stack2.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray2[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray2[x],true);
		}		
	}

	private void updateTextStack3(){
		int[] stackArray = stack3.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray3[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray3[x],true);
		}
	}

	private void updateTextStack4(){
		int[] stackArray = stack4.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray4[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray4[x],true);
		}		
	}

	private void updateTextStack5(){
		int[] stackArray = stack5.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray5[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray5[x],true);
		}		
	}

	private void updateTextStack6(){
		int[] stackArray = stack6.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray6[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray6[x],true);
		}		
	}

	private void updateTextStack7(){
		int[] stackArray = stack7.getCardsArray();
		for(int x=0;x<17;x++){
			if(x<stackArray.length)
				this.updateButton(buttonArray7[stackArray.length-x-1],stackArray[x]);
			else
				resetButton(buttonArray7[x],true);
		}		
	}

	private void updateTextSuite1(){
		if(!suite1.isEmpty())
			this.updateButton(suiteButton1,suite1.getTop());
		else
			this.resetButton(suiteButton1,false);
	}

	private void updateTextSuite2(){
		if(!suite2.isEmpty())
			this.updateButton(suiteButton2,suite2.getTop());
		else
			this.resetButton(suiteButton2,false);
	}

	private void updateTextSuite3(){
		if(!suite3.isEmpty())
			this.updateButton(suiteButton3,suite3.getTop());
		else
			this.resetButton(suiteButton3,false);
	}

	private void updateTextSuite4(){
		if(!suite4.isEmpty())
			this.updateButton(suiteButton4,suite4.getTop());
		else{
			this.resetButton(suiteButton4,false);
		}
	}

	private void updateTextDeck1(){
		if(!deck1.isEmpty()){
			this.updateButton(deckButton1,0);
			deckButton1.setText("Draw");
		}
		else{
			this.resetButton(deckButton1,false);
			deckButton1.setText("Reset");
		}
	}

	private void updateTextDeck2(){
		if(!deck2.isEmpty())
			this.updateButton(deckButton2,deck2.getTop());
		else
			this.resetButton(deckButton2,false);
	}

	private void resetVar(){
		this.pass = false;
		this.answer1 = 0;
		this.answer2 = 0;
		this.resetBorder();
	}

	private Boolean checkAce(){
		return this.answer2==11||this.answer2==22||this.answer2==33||this.answer2==44;
	}

	private void resetBorder(){
		stackButton1.setBorder(new LineBorder(Color.BLACK));
		stackButton2.setBorder(new LineBorder(Color.BLACK));
		stackButton3.setBorder(new LineBorder(Color.BLACK));
		stackButton4.setBorder(new LineBorder(Color.BLACK));
		stackButton5.setBorder(new LineBorder(Color.BLACK));
		stackButton6.setBorder(new LineBorder(Color.BLACK));
		stackButton7.setBorder(new LineBorder(Color.BLACK));
		suiteButton1.setBorder(new LineBorder(Color.BLACK));
		suiteButton2.setBorder(new LineBorder(Color.BLACK));
		suiteButton3.setBorder(new LineBorder(Color.BLACK));
		suiteButton4.setBorder(new LineBorder(Color.BLACK));
		deckButton2.setBorder(new LineBorder(Color.BLACK));
	}

	private void checkCommand(){
		if(this.answer1 == 0){
			exchangeDeck();
		}
		else if(this.answer1 == 11){
			suiteToStack1(this.answer2);
		}
		else if(this.answer1 == 22){
			suiteToStack2(this.answer2);
		}
		else if(this.answer1 == 33){
			suiteToStack3(this.answer2);
		}
		else if(this.answer1 == 44){
			suiteToStack4(this.answer2);
		}
		else if((this.answer1 == 1)&&(this.checkAce())){
			stackToSuite1(this.answer2);
		}
		else if((this.answer1 == 2)&&(this.checkAce())){
			stackToSuite2(this.answer2);
		}
		else if((this.answer1 == 3)&&(this.checkAce())){
			stackToSuite3(this.answer2);
		}
		else if((this.answer1 == 4)&&(this.checkAce())){
			stackToSuite4(this.answer2);
		}
		else if((this.answer1 == 5)&&(this.checkAce())){
			stackToSuite5(this.answer2);
		}
		else if((this.answer1 == 6)&&(this.checkAce())){
			stackToSuite6(this.answer2);
		}
		else if((this.answer1 == 7)&&(this.checkAce())){
			stackToSuite7(this.answer2);
		}
		else if(this.answer1 == 1){
			stackToStack1(this.answer2);
		}
		else if(this.answer1 == 2){
			stackToStack2(this.answer2);
		}
		else if(this.answer1 == 3){
			stackToStack3(this.answer2);
		}
		else if(this.answer1 == 4){
			stackToStack4(this.answer2);
		}
		else if(this.answer1 == 5){
			stackToStack5(this.answer2);
		}
		else if(this.answer1 == 6){
			stackToStack6(this.answer2);
		}
		else if(this.answer1 == 7){
			stackToStack7(this.answer2);
		}
		else if(this.answer1 == 9){
			if(this.checkAce()){
				putToSuite(this.answer2);
			}
			else{
				putToStack(this.answer2);
			}
		}
		else{
			pass = false;
		}
		cardOpener();
	}

	public void update(){
		this.checkCommand();
		this.updateView();
		this.resetVar();
	}

	public void actionPerformed(ActionEvent event){
		if(event.getSource()==deckButton1){
			this.answer1 = 0;
			this.answer2 = 0;
			this.update();
		}
		else if(event.getSource()==deckButton2){
			if(!this.pass){
				deckButton2.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 9;
				this.pass = true;
			}
			else{
				if(this.answer1==9){
					this.resetVar();
				}
				else{
					this.answer2 = 9;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton1){
			if(!this.pass){
				stackButton1.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 1;
				this.pass = true;
			}
			else{
				if(this.answer1==1){
					this.resetVar();
				}
				else{
					this.answer2 = 1;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton2){
			if(!this.pass){
				stackButton2.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 2;
				this.pass = true;
			}
			else{
				if(this.answer1==2){
					this.resetVar();
				}
				else{
					this.answer2 = 2;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton3){
			if(!this.pass){
				stackButton3.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 3;
				this.pass = true;
			}
			else{
				if(this.answer1==3){
					this.resetVar();
				}
				else{
					this.answer2 = 3;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton4){
			if(!this.pass){
				stackButton4.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 4;
				this.pass = true;
			}
			else{
				if(this.answer1==4){
					this.resetVar();
				}
				else{
					this.answer2 = 4;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton5){
			if(!this.pass){
				stackButton5.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 5;
				this.pass = true;
			}
			else{
				if(this.answer1==5){
					this.resetVar();
				}
				else{
					this.answer2 = 5;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton6){
			if(!this.pass){
				stackButton6.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 6;
				this.pass = true;
			}
			else{
				if(this.answer1==6){
					this.resetVar();
				}
				else{
					this.answer2 = 6;
					this.update();
				}
			}
		}
		else if(event.getSource()==stackButton7){
			if(!this.pass){
				stackButton7.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 7;
				this.pass = true;
			}
			else{
				if(this.answer1==7){
					this.resetVar();
				}
				else{
					this.answer2 = 7;
					this.update();
				}
			}
		}
		else if(event.getSource()==suiteButton1){
			if(!this.pass){
				suiteButton1.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 11;
				this.pass = true;
			}
			else{
				if(this.answer1==11){
					this.resetVar();
				}
				else{
					this.answer2 = 11;
					this.update();
				}
			}
		}
		else if(event.getSource()==suiteButton2){
			if(!this.pass){
				suiteButton2.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 22;
				this.pass = true;
			}
			else{
				if(this.answer1==22){
					this.resetVar();
				}
				else{
					this.answer2 = 22;
					this.update();
				}
			}
		}
		else if(event.getSource()==suiteButton3){
			if(!this.pass){
				suiteButton3.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 33;
				this.pass = true;
			}
			else{
				if(this.answer1==33){
					this.resetVar();
				}
				else{
					this.answer2 = 33;
					this.update();
				}
			}
		}
		else if(event.getSource()==suiteButton4){
			if(!this.pass){
				suiteButton4.setBorder(new BevelBorder(BevelBorder.RAISED,Color.YELLOW,Color.YELLOW));
				this.answer1 = 44;
				this.pass = true;
			}
			else{
				if(this.answer1==44){
					this.resetVar();
				}
				else{
					this.answer2 = 44;
					this.update();
				}
			}
		}
		else if(event.getSource()==newGameButton){
			newGame();
			cardOpener();
			this.answer1=99;
			this.answer2=99;
			this.update();
		}
		else if(event.getSource()==checkGameButton){
			if(isWinner()){
				JOptionPane.showMessageDialog(null, "Congrats, You Win!");
			}
			else{
				JOptionPane.showMessageDialog(null, "No yet a winner, just play.");	
			}
		}
		else if(event.getSource()==quitGameButton){
			System.exit(0);	
		}
	}
}