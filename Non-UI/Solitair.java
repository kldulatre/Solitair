/*
	Kenjie Lloyd Dulatre
	2014-28334
	April 18, 2016
	CS12 MP1 - Solitair Console Code
	under Prof. Mario Carreon
*/

import java.util.*;
import java.util.Scanner;
import java.io.*;

public class Solitair{
	Integer[] deck = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52};
	int[] s1,s2,s3,s4,s5,s6,s7;
	int ss1,ss2,ss3,ss4,ss5,ss6,ss7;
	int[] b1,b2,b3,b4;
	int bb1,bb2,bb3,bb4;
	int[] c1,c2;
	int cc1,cc2;
	
	public Integer[] newGame(){
		this.shuffleCards();
		this.distributeCards();
		this.b1 = this.b2 = this.b3 = this.b4 = new int[0];
		this.bb1 = this.bb2 = this.bb3 = this.bb4 = 0;
		return this.deck;
	}

	public void openCard(){
		if(this.s1.length!=0){
			if(this.ss1==0) this.ss1++;
		}
		if(this.s2.length!=0){
			if(this.ss2==0) this.ss2++;
		}
		if(this.s3.length!=0){
			if(this.ss3==0) this.ss3++;
		}
		if(this.s4.length!=0){
			if(this.ss4==0) this.ss4++;
		}
		if(this.s5.length!=0){
			if(this.ss5==0) this.ss5++;
		}
		if(this.s6.length!=0){
			if(this.ss6==0) this.ss6++;
		}
		if(this.s7.length!=0){
			if(this.ss7==0) this.ss7++;
		}
	}

	public void swap(int x, int y){
		Integer temp = deck[x];
		deck[x] = deck[y];
		deck[y] = temp;
	}

	public void shuffleCards(){
		Random random = new Random();
		for(int x=0;x<=52;x++){
			this.swap(random.nextInt(52),random.nextInt(52));
		}
	}

	public int[] addCard(int[] temp,Integer value){
		temp = Arrays.copyOf(temp, temp.length +1);
		temp[temp.length - 1] = value;
		return temp;
	}

	public int[] removeCard(int[] temp){
		temp = Arrays.copyOf(temp,temp.length-1);
		return temp; 
	}

	public int stackKingChecker(int[] temp1a,int num1a,int[] temp2a){
		boolean pass = false;
		int counter = 1;
		if(temp2a.length!=0){
			return 0;
		}
		while(true){
			if(temp1a[temp1a.length-counter]==13||temp1a[temp1a.length-counter]==26||temp1a[temp1a.length-counter]==39||temp1a[temp1a.length-counter]==52){
				return counter;
			}
			else if(counter>=num1a){
				break;
			}
			else{
				counter++;
			}
		}
		return 0;
	}

	public void stackKingMove(String m1,String m2){
		if(this.check(m1,"1")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s1[this.s1.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s1[this.s1.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s1[this.s1.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s1[this.s1.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s1[this.s1.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s1,this.ss1,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s1[this.s1.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}

		else if(this.check(m1,"2")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s2[this.s2.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s2[this.s2.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s2[this.s2.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s2[this.s2.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s2[this.s2.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s2,this.ss2,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s2[this.s2.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}

		else if(this.check(m1,"3")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s3[this.s3.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s3[this.s3.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s3[this.s3.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s3[this.s3.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s3[this.s3.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s3,this.ss3,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s3[this.s3.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}

		else if(this.check(m1,"4")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s4[this.s4.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s4[this.s4.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s4[this.s4.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s4[this.s4.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s4[this.s4.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s4,this.ss4,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s4[this.s4.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}

		else if(this.check(m1,"5")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s5[this.s5.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s5[this.s5.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s5[this.s5.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s5[this.s5.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s5[this.s5.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s5,this.ss5,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s5[this.s5.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}

		else if(this.check(m1,"6")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s6[this.s6.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s6[this.s6.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s6[this.s6.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s6[this.s6.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s6[this.s6.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"7")){
			int counter = this.stackKingChecker(this.s6,this.ss6,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s6[this.s6.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}

		else if(this.check(m1,"7")&& this.check(m2,"1")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s7[this.s7.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"2")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s7[this.s7.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"3")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s7[this.s7.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"4")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s7[this.s7.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"5")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s7[this.s7.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"6")){
			int counter = this.stackKingChecker(this.s7,this.ss7,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s7[this.s7.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
	}

	public int stackChecker(int[] temp1,int temp11,int[] temp2){
		boolean pass = false;
		int counter = 1;
		while(true){
			pass = checker(temp2[temp2.length-1],temp1[temp1.length-counter]);
			if(pass){
				return counter;
			}
			else if(counter>=temp11){
				break;
			}
			else{
				counter++;
			}
		}
		return 0;
	}

	public void stackMove(String m1,String m2){
		if(this.check(m1,"1")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s1[this.s1.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s1[this.s1.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s1[this.s1.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s1[this.s1.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s1[this.s1.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}
		else if(this.check(m1,"1")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s1,this.ss1,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s1[this.s1.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s1 = this.removeCard(this.s1);
				this.ss1--;
			}
		}

		else if(this.check(m1,"2")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s2[this.s2.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s2[this.s2.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s2[this.s2.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s2[this.s2.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s2[this.s2.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}
		else if(this.check(m1,"2")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s2,this.ss2,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s2[this.s2.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s2 = this.removeCard(this.s2);
				this.ss2--;
			}
		}

		else if(this.check(m1,"3")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s3[this.s3.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s3[this.s3.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s3[this.s3.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s3[this.s3.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s3[this.s3.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}
		else if(this.check(m1,"3")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s3,this.ss3,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s3[this.s3.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s3 = this.removeCard(this.s3);
				this.ss3--;
			}
		}

		else if(this.check(m1,"4")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s4[this.s4.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s4[this.s4.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s4[this.s4.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s4[this.s4.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s4[this.s4.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}
		else if(this.check(m1,"4")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s4,this.ss4,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s4[this.s4.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s4 = this.removeCard(this.s4);
				this.ss4--;
			}
		}

		else if(this.check(m1,"5")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s5[this.s5.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s5[this.s5.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s5[this.s5.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s5[this.s5.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s5[this.s5.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}
		else if(this.check(m1,"5")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s5,this.ss5,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s5[this.s5.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s5 = this.removeCard(this.s5);
				this.ss5--;
			}
		}

		else if(this.check(m1,"6")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s6[this.s6.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s6[this.s6.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s6[this.s6.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s6[this.s6.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s6[this.s6.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}
		else if(this.check(m1,"6")&& this.check(m2,"7")){
			int counter = this.stackChecker(this.s6,this.ss6,this.s7);
			for(int x=counter;x>=1;x--){
				this.s7 = this.addCard(this.s7,this.s6[this.s6.length-x]);
				this.ss7++;
			}
			for(int x=counter;x>=1;x--){
				this.s6 = this.removeCard(this.s6);
				this.ss6--;
			}
		}

				else if(this.check(m1,"7")&& this.check(m2,"1")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s1);
			for(int x=counter;x>=1;x--){
				this.s1 = this.addCard(this.s1,this.s7[this.s7.length-x]);
				this.ss1++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"2")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s2);
			for(int x=counter;x>=1;x--){
				this.s2 = this.addCard(this.s2,this.s7[this.s7.length-x]);
				this.ss2++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"3")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s3);
			for(int x=counter;x>=1;x--){
				this.s3 = this.addCard(this.s3,this.s7[this.s7.length-x]);
				this.ss3++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"4")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s4);
			for(int x=counter;x>=1;x--){
				this.s4 = this.addCard(this.s4,this.s7[this.s7.length-x]);
				this.ss4++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"5")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s5);
			for(int x=counter;x>=1;x--){
				this.s5 = this.addCard(this.s5,this.s7[this.s7.length-x]);
				this.ss5++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
		else if(this.check(m1,"7")&& this.check(m2,"6")){
			int counter = this.stackChecker(this.s7,this.ss7,this.s6);
			for(int x=counter;x>=1;x--){
				this.s6 = this.addCard(this.s6,this.s7[this.s7.length-x]);
				this.ss6++;
			}
			for(int x=counter;x>=1;x--){
				this.s7 = this.removeCard(this.s7);
				this.ss7--;
			}
		}
	}

	public Boolean check(String move1,String myChar){
		if(move1.trim().equalsIgnoreCase(myChar)){
			return true;
		}
		else return false;
	}

	public void addRemove(String m1, Integer value ,String m2, boolean mb1, boolean mb2){
		if(m1.equals("c1")&&mb1==false) this.c1 = this.addCard(this.c1,value);
		else if(m1.equals("c1")&&mb1==true){
			this.c1 = this.addCard(this.c1,value);
			this.cc1++;
		}

		else if(m1.equals("c2")&&mb1==false) this.c2 = this.addCard(this.c2,value);
		else if(m1.equals("c2")&&mb1==true){
			this.c2 = this.addCard(this.c2,value);
			this.cc2++;
		}
		
		else if(m1.equals("b1")&&mb1==false) this.b1 = this.addCard(this.b1,value);
		else if(m1.equals("b1")&&mb1==true){
			this.b1 = this.addCard(this.b1,value);
			this.bb1++;
		}
		
		else if(m1.equals("b2")&&mb1==false) this.b2 = this.addCard(this.b2,value);
		else if(m1.equals("b2")&&mb1==true){
			this.b2 = this.addCard(this.b2,value);
			this.bb2++;
		}
		
		else if(m1.equals("b3")&&mb1==false) this.b3 = this.addCard(this.b3,value);
		else if(m1.equals("b3")&&mb1==true){
			this.b3 = this.addCard(this.b3,value);
			this.bb3++;
		}
		
		else if(m1.equals("b4")&&mb1==false) this.b4 = this.addCard(this.b4,value);
		else if(m1.equals("b4")&&mb1==true){
			this.b4 = this.addCard(this.b4,value);
			this.bb4++;
		}
		
		else if(m1.equals("s1")&&mb1==false) this.s1 = this.addCard(this.s1,value);
		else if(m1.equals("s1")&&mb1==true){
			this.s1 = this.addCard(this.s1,value);
			this.ss1++;
		}
		
		else if(m1.equals("s2")&&mb1==false) this.s2 = this.addCard(this.s2,value);
		else if(m1.equals("s2")&&mb1==true){
			this.s2 = this.addCard(this.s2,value);
			this.ss2++;
		}
		
		else if(m1.equals("s3")&&mb1==false) this.s3 = this.addCard(this.s3,value);
		else if(m1.equals("s3")&&mb1==true){
			this.s3 = this.addCard(this.s3,value);
			this.ss3++;
		}
		
		else if(m1.equals("s4")&&mb1==false) this.s4 = this.addCard(this.s4,value);
		else if(m1.equals("s4")&&mb1==true){
			this.s4 = this.addCard(this.s4,value);
			this.ss4++;
		}
		
		else if(m1.equals("s5")&&mb1==false) this.s5 = this.addCard(this.s5,value);
		else if(m1.equals("s5")&&mb1==true){
			this.s5 = this.addCard(this.s5,value);
			this.ss5++;
		}
		
		else if(m1.equals("s6")&&mb1==false) this.s6 = this.addCard(this.s6,value);
		else if(m1.equals("s6")&&mb1==true){
			this.s6 = this.addCard(this.s6,value);
			this.ss6++;
		}
		
		else if(m1.equals("s7")&&mb1==false) this.s7 = this.addCard(this.s7,value);
		else if(m1.equals("s7")&&mb1==true){
			this.s7 = this.addCard(this.s7,value);
			this.ss7++;
		}

		if(m2.equals("c1")&&mb2==false) this.c1 = this.removeCard(this.c1);
		else if(m2.equals("c1")&&mb2==true){ 
			this.c1 = this.removeCard(this.c1);
			this.cc1--;
		}
		else if(m2.equals("c2")&&mb2==false) this.c2 = this.removeCard(this.c2);
		else if(m2.equals("c2")&&mb2==true){ 
			this.c2 = this.removeCard(this.c2);
			this.cc2--;
		}

		else if(m2.equals("b1")&&mb2==false) this.b1 = this.removeCard(this.b1);
		else if(m2.equals("b1")&&mb2==true){ 
			this.b1 = this.removeCard(this.b1);
			this.bb1--;
		}
		
		else if(m2.equals("b2")&&mb2==false) this.b2 = this.removeCard(this.b2);
		else if(m2.equals("b2")&&mb2==true){
			this.b2 = this.removeCard(this.b2);
			this.bb2--;
		}
		
		else if(m2.equals("b3")&&mb2==false) this.b3 = this.removeCard(this.b3);
		else if(m2.equals("b3")&&mb2==true){ 
			this.b3 = this.removeCard(this.b3);
			this.bb3--;
		}

		else if(m2.equals("b4")&&mb2==false) this.b4 = this.removeCard(this.b4);
		else if(m2.equals("b4")&&mb2==true){ 
			this.b4 = this.removeCard(this.b4);
			this.bb4--;
		}


		else if(m2.equals("s1")&&mb2==false) this.s1 = this.removeCard(this.s1);
		else if(m2.equals("s1")&&mb2==true){ 
			this.s1 = this.removeCard(this.s1);
			this.ss1--;
		}


		else if(m2.equals("s2")&&mb2==false) this.s2 = this.removeCard(this.s2);
		else if(m2.equals("s2")&&mb2==true){ 
			this.s2 = this.removeCard(this.s2);
			this.ss2--;
		}


		else if(m2.equals("s3")&&mb2==false) this.s3 = this.removeCard(this.s3);
		else if(m2.equals("s3")&&mb2==true){ 
			this.s3 = this.removeCard(this.s3);
			this.ss3--;
		}


		else if(m2.equals("s4")&&mb2==false) this.s4 = this.removeCard(this.s4);
		else if(m2.equals("s4")&&mb2==true){ 
			this.s4 = this.removeCard(this.s4);
			this.ss4--;
		}


		else if(m2.equals("s5")&&mb2==false) this.s5 = this.removeCard(this.s5);
		else if(m2.equals("s5")&&mb2==true){ 
			this.s5 = this.removeCard(this.s5);
			this.ss5--;
		}

		else if(m2.equals("s6")&&mb2==false) this.s6 = this.removeCard(this.s6);
		else if(m2.equals("s6")&&mb2==true){ 
			this.s6 = this.removeCard(this.s6);
			this.ss6--;
		}

		else if(m2.equals("s7")&&mb2==false) this.s7 = this.removeCard(this.s7);
		else if(m2.equals("s7")&&mb2==true){ 
			this.s7 = this.removeCard(this.s7);
			this.ss6--;
		}

	}

	public String getCard(int cardNum){
		String card = new String();
		
		if((cardNum-1)/13==0){
			card += "SB";
		}
		else if((cardNum-1)/13==1){
			card += "HR";
		}
		else if((cardNum-1)/13==2){
			card += "DR";
		}
		else if((cardNum-1)/13==3){
			card += "CB";
		}

		if(cardNum%13==0){
			card += "K";
		}
		else if(cardNum%13<=10){
			card += Integer.toString(cardNum%13);
		}
		else if(cardNum%13==11){
			card += "J";
		}
		else if(cardNum%13==12){
			card += "Q";
		}
		
		if(cardNum%13!=10){
			card = " " + card;
		}

		return card;
	}

	public static Boolean checkColor(int card1,int card2){
		int pass1=0,pass2=0; 
		
		if((1<=card1 && card1<=13) || (40<=card1 && card1<=52)){
			pass1 = 1;
		}
		else{
			pass1 = 2;
		}

		if((1<=card2 && card2<=13) || (40<=card2 && card2<=52)){
			pass2 = 1;
		}
		else{
			pass2 = 2;
		}

		if(pass1!=pass2){
			return true;
		}
		else{
			return false;
		}
	}

	public static Boolean checkNum(int card1,int card2){
		if( (card2-1)%13 >= (card1-1)%13 ) {
			return false;
		}
		else {
			if((card1-1)%13-1==(card2-1)%13){
				return true;
			}
			else{
				return false;
			}
		}
	}

	public Boolean checker(int card1,int card2){ //card1 ay mas malaki kay card2
		Boolean pass1 = this.checkColor(card1,card2);
		Boolean pass2 = this.checkNum(card1,card2);
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}

	public static Boolean checkSuiteType(int card1,int card2){
		if((card1-1)/13==(card2-1)/13){
			return true;
		}	
		else{
			return false;
		}
	}

	public static Boolean checkSuiteNum(int card1,int card2){
		if( (card2-1)%13 < (card1-1)%13 ) {
			return false;
		}
		else {
			if((card1-1)%13==(card2-1)%13-1){
				return true;
			}
			else{
				return false;
			}
		}
	}

	public Boolean checkSuite(int card1,int card2){
		Boolean pass1 = this.checkSuiteType(card1,card2);
		Boolean pass2 = this.checkSuiteNum(card1,card2);
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}

	public void distributeCards(){
		this.s1 = this.s2 = this.s3 = this.s4 = this.s5 = this.s6 = this.s7 = new int[0];
		this.ss1 = this.ss2 = this.ss3 = this.ss4 = this.ss5 = this.ss6 = this.ss7 = 0;
		this.c1 = this.c2 = new int[0];
		this.cc1 = this.cc2 = 0;
		for(int x=0;x<52;x++){
			if(x<1){
				this.s1 = addCard(this.s1,this.deck[x]);	
			}
			else if(x<3){
				this.s2 = addCard(this.s2,this.deck[x]);
			}
			else if(x<6){
				this.s3 = addCard(this.s3,this.deck[x]);
			}
			else if(x<10){
				this.s4 = addCard(this.s4,this.deck[x]);
			}
			else if(x<15){
				this.s5 = addCard(this.s5,this.deck[x]);
			}
			else if(x<21){
				this.s6 = addCard(this.s6,this.deck[x]);
			}
			else if(x<28){
				this.s7 = addCard(this.s7,this.deck[x]);
			}
			else{
				this.c1 = addCard(this.c1,this.deck[x]);
				this.cc1++;
			}
		}
	}

	public boolean checkIfWin(){
		boolean pass1 = this.s1.length==0 || this.s1.length-this.ss1==0;
		boolean pass2 = this.s2.length==0 || this.s2.length-this.ss2==0;
		boolean pass3 = this.s3.length==0 || this.s3.length-this.ss3==0;
		boolean pass4 = this.s4.length==0 || this.s4.length-this.ss4==0;
		boolean pass5 = this.s5.length==0 || this.s5.length-this.ss5==0;
		boolean pass6 = this.s6.length==0 || this.s6.length-this.ss6==0;
		boolean pass7 = this.s7.length==0 || this.s7.length-this.ss6==0;
		if(pass1 && pass2 && pass3 && pass4 && pass5 && pass6 && pass7){
			return true;
		}
		else{
			return false;
		}
	}

	public void printBoard(){

		String suite = "    ";
		System.out.println("    CS12 Solitaire by Kenjie Lloyd Dulatre");
		System.out.println("    V - Checking\tN - New Game\tC - Quit Game");
		System.out.println("\n");
		System.out.println("       A      S      D      F             Z      X");
		try{
			suite += getCard(this.b1[this.b1.length-1])+"   ";
		}catch(Exception e){
			suite += "   x   ";
		}
		try{
			suite += getCard(this.b2[this.b2.length-1])+"   ";
		}catch(Exception e){
			suite += "   x   ";
		}
		try{
			suite += getCard(this.b3[this.b3.length-1])+"   ";
		}catch(Exception e){
			suite += "   x   ";
		}
		try{
			suite += getCard(this.b4[this.b4.length-1])+"   ";
		}catch(Exception e){
			suite += "   x   ";
		}
		suite += "       ";
		try{
			suite += getCard(this.c2[this.cc2-1])+"   ";
		}catch(Exception e){
			suite += "       ";
		}
		if(this.cc1>=1){
			suite += "xxxx";
		}
		else{
			suite += "       ";	
		}
		
		System.out.println(suite+"\n");

		String string = new String();
		System.out.println("       1      2      3      4      5      6      7");
		
		for(int x=0;x<18;x++){
			if(x+1>=10){
				string = "    ";
			}
			else{
				string = "    ";	
			}
			try{
				if(s1.length-ss1<x+1){
					string += getCard(s1[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";
			
			try{
				if(s2.length-ss2<x+1){
					string += getCard(s2[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";
			
			try{
				if(s3.length-ss3<x+1){
					string += getCard(s3[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";

			try{
				if(s4.length-ss4<x+1){
					string += getCard(s4[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";

			try{
				if(s5.length-ss5<x+1){
					string += getCard(s5[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";

			try{
				if(s6.length-ss6<x+1){
					string += getCard(s6[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			string += "   ";

			try{
				if(s7.length-ss7<x+1){
					string += getCard(s7[x]);
				}
				else{
					string += "   x";
				}
			}catch(Exception e){
				string += "    ";
			}

			System.out.println(string);
		}
	}

}