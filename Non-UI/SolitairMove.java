/*
	Kenjie Lloyd Dulatre
	2014-28334
	April 18, 2016
	CS12 MP1 - Solitair Console Code
	under Prof. Mario Carreon
*/

public class SolitairMove{
	public static Boolean moveCheck(String move1, String move2){
		boolean pass1 = false;
		boolean pass2 = false;

		if(move1.trim().equalsIgnoreCase("1")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("2")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("3")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("4")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("5")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("6")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("7")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("A")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("S")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("D")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("F")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("Z")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("X")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("C")) pass1 = true;

		if(move2.trim().equalsIgnoreCase("1")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("2")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("3")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("4")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("5")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("6")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("7")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("A")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("S")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("D")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("F")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("Z")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("X")) pass2 = true;
		else if(move2.trim().equalsIgnoreCase("C")) pass2 = true;
		
		if(move1.trim().equalsIgnoreCase(move2)) pass1 = false;
		
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static Boolean moveCheck(String move1,boolean pass2){
		boolean pass1 = false;

		if(move1.trim().equalsIgnoreCase("X")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("C")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("V")) pass1 = true;
		else if(move1.trim().equalsIgnoreCase("B")) pass1 = true;
		
		if(pass1&&pass2){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static Boolean check(String move1,String myChar){
		if(move1.trim().equalsIgnoreCase(myChar)){
			return true;
		}
		else return false;
	}

	public static Boolean moveNumbers(String move1,String move2){
		Boolean pass1 = false;
		Boolean pass2 = false;

		if(check(move1,"1")) pass1 = true;
		else if(check(move1,"2")) pass1 = true;
		else if(check(move1,"4")) pass1 = true;
		else if(check(move1,"3")) pass1 = true;
		else if(check(move1,"5")) pass1 = true;
		else if(check(move1,"6")) pass1 = true;
		else if(check(move1,"7")) pass1 = true;

		if(check(move2,"1")) pass2 = true;
		else if(check(move2,"2")) pass2 = true;
		else if(check(move2,"3")) pass2 = true;
		else if(check(move2,"4")) pass2 = true;
		else if(check(move2,"5")) pass2 = true;
		else if(check(move2,"6")) pass2 = true;
		else if(check(move2,"7")) pass2 = true;

		if(pass1 && pass2){
			return true;
		}
		else return false;
	}
}