/*
	Kenjie Lloyd Dulatre
	2014-28334
	April 18, 2016
	CS12 MP1 - Solitair Console Code
	under Prof. Mario Carreon
*/

import java.util.*;
import java.util.Scanner;
import java.io.*;

public class CS12MP1 extends SolitairMove{
	public static void main(String[] args){
		Scanner scanner1 = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);	
		
		Solitair game = new Solitair();
		Integer deck[] = game.newGame();
		
		Boolean pass = true;
		Boolean err = false;

		Boolean winner = false;
		Boolean winnerPass = false;

		while(pass){
			game.openCard();

			try{
				System.out.print("\033[H\033[2J"); 
			    System.out.flush();
			}
			catch(Exception e){
				continue;
			}
			
			game.printBoard();
			
			if(winner){
				if(winnerPass){
					System.out.println("    Congratulation You've Win the Game!");	
				}
				else{
					System.out.println("    Sorry not yet a winner.");
				}
				winner = false;
				winnerPass = false;
			}

			if(err){
				System.out.println("    Please input properly!");
				err = false;
			}

			System.out.print("    From: ");
			String move1 = scanner1.nextLine();
			
			if(moveCheck(move1,true)){
				if(check(move1,"C")) pass = false;
				else if(check(move1,"V")){
					winner = true;
					winnerPass = game.checkIfWin();

				}
				else if(check(move1,"B")){
					game.newGame();
				}
				else{
					if(game.cc1==0){
						for(int x = game.cc2;0<game.cc2;game.cc2--){
							game.addRemove("c1",game.c2[game.cc2-1],"c2",true,false);
						}
					}
					else game.addRemove("c2",game.c1[game.cc1-1],"c1",true,true);
				}
				continue;
			}

			System.out.print("    To: ");
			String move2 = scanner2.nextLine();
			
if(moveCheck(move1,move2)){
	if(check(move1,"Z")){
		// if(game.cc2==0) err = true;
		
		if(check(move2,"1")){
			if(game.s1.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s1",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s1[game.s1.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s1",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"2")){
			if(game.s2.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s2",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s2[game.s2.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s2",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"3")){
			if(game.s3.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s3",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s3[game.s3.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s3",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"4")){
			if(game.s4.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s4",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s4[game.s4.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s4",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"5")){
			if(game.s5.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s5",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s5[game.s5.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s5",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"6")){
			if(game.s6.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s6",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s6[game.s6.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s6",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"7")){
			if(game.s7.length==0){
				if(game.c2[game.c2.length-1]==13||game.c2[game.c2.length-1]==26||game.c2[game.c2.length-1]==39||game.c2[game.c2.length-1]==52){
					game.addRemove("s7",game.c2[game.c2.length-1],"c2",true,true);
				}
			}
			else if(game.checker(game.s7[game.s7.length-1],game.c2[game.c2.length-1])){
				game.addRemove("s7",game.c2[game.c2.length-1],"c2",true,true);
			}
			else err = true;
		}

		else if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.c2[game.c2.length-1]==1||game.c2[game.c2.length-1]==14||game.c2[game.c2.length-1]==27||game.c2[game.c2.length-1]==40){
					game.addRemove("b1",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.c2[game.c2.length-1]);
				System.out.println(passAdd);
				if(passAdd){
					game.addRemove("b1",game.c2[game.c2.length-1],"c2",true,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.c2[game.c2.length-1]==1||game.c2[game.c2.length-1]==14||game.c2[game.c2.length-1]==27||game.c2[game.c2.length-1]==40){
					game.addRemove("b2",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.c2[game.c2.length-1]);
				System.out.println(passAdd);
				if(passAdd){
					game.addRemove("b2",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.c2[game.c2.length-1]==1||game.c2[game.c2.length-1]==14||game.c2[game.c2.length-1]==27||game.c2[game.c2.length-1]==40){
					game.addRemove("b3",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.c2[game.c2.length-1]);
				System.out.println(passAdd);
				if(passAdd){
					game.addRemove("b3",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.c2[game.c2.length-1]==1||game.c2[game.c2.length-1]==14||game.c2[game.c2.length-1]==27||game.c2[game.c2.length-1]==40){
					game.addRemove("b4",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.c2[game.c2.length-1]);
				System.out.println(passAdd);
				if(passAdd){
					game.addRemove("b4",game.c2[game.c2.length-1],"c2",false,true);
				}
				else err = true;
			}
		}

	}

	else if(moveNumbers(move1,move2)){
		try{
			game.stackMove(move1,move2);
		}
		catch(Exception e){
			game.stackKingMove(move1,move2);
		}			
	}

	else if(check(move1,"1")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s1[game.s1.length-1]==1||game.s1[game.s1.length-1]==14||game.s1[game.s1.length-1]==27||game.s1[game.s1.length-1]==40){
					game.addRemove("b1",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s1[game.s1.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s1[game.s1.length-1]==1||game.s1[game.s1.length-1]==14||game.s1[game.s1.length-1]==27||game.s1[game.s1.length-1]==40){
					game.addRemove("b2",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s1[game.s1.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s1[game.s1.length-1]==1||game.s1[game.s1.length-1]==14||game.s1[game.s1.length-1]==27||game.s1[game.s1.length-1]==40){
					game.addRemove("b3",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s1[game.s1.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s1[game.s1.length-1]==1||game.s1[game.s1.length-1]==14||game.s1[game.s1.length-1]==27||game.s1[game.s1.length-1]==40){
					game.addRemove("b4",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s1[game.s1.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s1[game.s1.length-1],"s1",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"2")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s2[game.s2.length-1]==1||game.s2[game.s2.length-1]==14||game.s2[game.s2.length-1]==27||game.s2[game.s2.length-1]==40){
					game.addRemove("b1",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s2[game.s2.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s2[game.s2.length-1]==1||game.s2[game.s2.length-1]==14||game.s2[game.s2.length-1]==27||game.s2[game.s2.length-1]==40){
					game.addRemove("b2",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s2[game.s2.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s2[game.s2.length-1]==1||game.s2[game.s2.length-1]==14||game.s2[game.s2.length-1]==27||game.s2[game.s2.length-1]==40){
					game.addRemove("b3",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s2[game.s2.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s2[game.s2.length-1]==1||game.s2[game.s2.length-1]==14||game.s2[game.s2.length-1]==27||game.s2[game.s2.length-1]==40){
					game.addRemove("b4",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s2[game.s2.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s2[game.s2.length-1],"s2",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"3")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s3[game.s3.length-1]==1||game.s3[game.s3.length-1]==14||game.s3[game.s3.length-1]==27||game.s3[game.s3.length-1]==40){
					game.addRemove("b1",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s3[game.s3.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s3[game.s3.length-1]==1||game.s3[game.s3.length-1]==14||game.s3[game.s3.length-1]==27||game.s3[game.s3.length-1]==40){
					game.addRemove("b2",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s3[game.s3.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s3[game.s3.length-1]==1||game.s3[game.s3.length-1]==14||game.s3[game.s3.length-1]==27||game.s3[game.s3.length-1]==40){
					game.addRemove("b3",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s3[game.s3.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s3[game.s3.length-1]==1||game.s3[game.s3.length-1]==14||game.s3[game.s3.length-1]==27||game.s3[game.s3.length-1]==40){
					game.addRemove("b4",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s3[game.s3.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s3[game.s3.length-1],"s3",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"4")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s4[game.s4.length-1]==1||game.s4[game.s4.length-1]==14||game.s4[game.s4.length-1]==27||game.s4[game.s4.length-1]==40){
					game.addRemove("b1",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s4[game.s4.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s4[game.s4.length-1]==1||game.s4[game.s4.length-1]==14||game.s4[game.s4.length-1]==27||game.s4[game.s4.length-1]==40){
					game.addRemove("b2",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s4[game.s4.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s4[game.s4.length-1]==1||game.s4[game.s4.length-1]==14||game.s4[game.s4.length-1]==27||game.s4[game.s4.length-1]==40){
					game.addRemove("b3",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s4[game.s4.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s4[game.s4.length-1]==1||game.s4[game.s4.length-1]==14||game.s4[game.s4.length-1]==27||game.s4[game.s4.length-1]==40){
					game.addRemove("b4",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s4[game.s4.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s4[game.s4.length-1],"s4",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"5")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s5[game.s5.length-1]==1||game.s5[game.s5.length-1]==14||game.s5[game.s5.length-1]==27||game.s5[game.s5.length-1]==40){
					game.addRemove("b1",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s5[game.s5.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s5[game.s5.length-1]==1||game.s5[game.s5.length-1]==14||game.s5[game.s5.length-1]==27||game.s5[game.s5.length-1]==40){
					game.addRemove("b2",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s5[game.s5.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s5[game.s5.length-1]==1||game.s5[game.s5.length-1]==14||game.s5[game.s5.length-1]==27||game.s5[game.s5.length-1]==40){
					game.addRemove("b3",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s5[game.s5.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s5[game.s5.length-1]==1||game.s5[game.s5.length-1]==14||game.s5[game.s5.length-1]==27||game.s5[game.s5.length-1]==40){
					game.addRemove("b4",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s5[game.s5.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s5[game.s5.length-1],"s5",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"6")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s6[game.s6.length-1]==1||game.s6[game.s6.length-1]==14||game.s6[game.s6.length-1]==27||game.s6[game.s6.length-1]==40){
					game.addRemove("b1",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s6[game.s6.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s6[game.s6.length-1]==1||game.s6[game.s6.length-1]==14||game.s6[game.s6.length-1]==27||game.s6[game.s6.length-1]==40){
					game.addRemove("b2",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s6[game.s6.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s6[game.s6.length-1]==1||game.s6[game.s6.length-1]==14||game.s6[game.s6.length-1]==27||game.s6[game.s6.length-1]==40){
					game.addRemove("b3",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s6[game.s6.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s6[game.s6.length-1]==1||game.s6[game.s6.length-1]==14||game.s6[game.s6.length-1]==27||game.s6[game.s6.length-1]==40){
					game.addRemove("b4",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s6[game.s6.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s6[game.s6.length-1],"s6",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"7")){
		if(check(move2,"A")){
			if(game.b1.length==0){
				if(game.s7[game.s7.length-1]==1||game.s7[game.s7.length-1]==14||game.s7[game.s7.length-1]==27||game.s7[game.s7.length-1]==40){
					game.addRemove("b1",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b1[game.b1.length-1],game.s7[game.s7.length-1]);
				if(passAdd){
					game.addRemove("b1",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"S")){
			if(game.b2.length==0){
				if(game.s7[game.s7.length-1]==1||game.s7[game.s7.length-1]==14||game.s7[game.s7.length-1]==27||game.s7[game.s7.length-1]==40){
					game.addRemove("b2",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b2[game.b2.length-1],game.s7[game.s7.length-1]);
				if(passAdd){
					game.addRemove("b2",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"D")){
			if(game.b3.length==0){
				if(game.s7[game.s7.length-1]==1||game.s7[game.s7.length-1]==14||game.s7[game.s7.length-1]==27||game.s7[game.s7.length-1]==40){
					game.addRemove("b3",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b3[game.b3.length-1],game.s7[game.s7.length-1]);
				if(passAdd){
					game.addRemove("b3",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
		}

		else if(check(move2,"F")){
			if(game.b4.length==0){
				if(game.s7[game.s7.length-1]==1||game.s7[game.s7.length-1]==14||game.s7[game.s7.length-1]==27||game.s7[game.s7.length-1]==40){
					game.addRemove("b4",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
			else{
				Boolean passAdd = game.checkSuite(game.b4[game.b4.length-1],game.s7[game.s7.length-1]);
				if(passAdd){
					game.addRemove("b4",game.s7[game.s7.length-1],"s7",false,true);
				}
				else err = true;
			}
		}
	}

	else if(check(move1,"A")){
		if(check(move2,"1")){
			if(game.s1.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s1",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s1[game.s1.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s1",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"2")){
			if(game.s2.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s2",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s2[game.s2.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s2",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"3")){
			if(game.s3.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s3",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s3[game.s3.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s3",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"4")){
			if(game.s4.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s4",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s4[game.s4.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s4",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"5")){
			if(game.s5.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s5",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s5[game.s5.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s5",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"6")){
			if(game.s6.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s6",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s6[game.s6.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s6",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
		else if(check(move2,"7")){
			if(game.s7.length==0){
				if(game.b1[game.b1.length-1]==13||game.b1[game.b1.length-1]==26||game.b1[game.b1.length-1]==39||game.b1[game.b1.length-1]==52){
					game.addRemove("s7",game.b1[game.b1.length-1],"b1",true,false);
				}
			}
			else if(game.checker(game.s7[game.s7.length-1],game.b1[game.b1.length-1])){
				game.addRemove("s7",game.b1[game.b1.length-1],"b1",true,false);
			}
		}
	}

	else if(check(move1,"S")){
		if(check(move2,"1")){
			if(game.s1.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s1",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s1[game.s1.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s1",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"2")){
			if(game.s2.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s2",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s2[game.s2.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s2",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"3")){
			if(game.s3.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s3",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s3[game.s3.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s3",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"4")){
			if(game.s4.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s4",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s4[game.s4.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s4",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"5")){
			if(game.s5.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s5",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s5[game.s5.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s5",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"6")){
			if(game.s6.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s6",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s6[game.s6.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s6",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
		else if(check(move2,"7")){
			if(game.s7.length==0){
				if(game.b2[game.b2.length-1]==13||game.b2[game.b2.length-1]==26||game.b2[game.b2.length-1]==39||game.b2[game.b2.length-1]==52){
					game.addRemove("s7",game.b2[game.b2.length-1],"b2",true,false);
				}
			}
			else if(game.checker(game.s7[game.s7.length-1],game.b2[game.b2.length-1])){
				game.addRemove("s7",game.b2[game.b2.length-1],"b2",true,false);
			}
		}
	}

	else if(check(move1,"D")){
		if(check(move2,"1")){
			if(game.s1.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s1",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s1[game.s1.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s1",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"2")){
			if(game.s2.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s2",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s2[game.s2.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s2",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"3")){
			if(game.s3.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s3",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s3[game.s3.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s3",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"4")){
			if(game.s4.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s4",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s4[game.s4.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s4",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"5")){
			if(game.s5.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s5",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s5[game.s5.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s5",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"6")){
			if(game.s6.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s6",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s6[game.s6.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s6",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
		else if(check(move2,"7")){
			if(game.s7.length==0){
				if(game.b3[game.b3.length-1]==13||game.b3[game.b3.length-1]==26||game.b3[game.b3.length-1]==39||game.b3[game.b3.length-1]==52){
					game.addRemove("s7",game.b3[game.b3.length-1],"b3",true,false);
				}
			}
			else if(game.checker(game.s7[game.s7.length-1],game.b3[game.b3.length-1])){
				game.addRemove("s7",game.b3[game.b3.length-1],"b3",true,false);
			}
		}
	}

	else if(check(move1,"F")){
		if(check(move2,"1")){
			if(game.s1.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s1",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s1[game.s1.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s1",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"2")){
			if(game.s2.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s2",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s2[game.s2.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s2",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"3")){
			if(game.s3.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s3",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s3[game.s3.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s3",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"4")){
			if(game.s4.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s4",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s4[game.s4.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s4",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"5")){
			if(game.s5.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s5",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s5[game.s5.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s5",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"6")){
			if(game.s6.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s6",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s6[game.s6.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s6",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
		else if(check(move2,"7")){
			if(game.s7.length==0){
				if(game.b4[game.b4.length-1]==13||game.b4[game.b4.length-1]==26||game.b4[game.b4.length-1]==39||game.b4[game.b4.length-1]==52){
					game.addRemove("s7",game.b4[game.b4.length-1],"b4",true,false);
				}
			}
			else if(game.checker(game.s7[game.s7.length-1],game.b4[game.b4.length-1])){
				game.addRemove("s7",game.b4[game.b4.length-1],"b4",true,false);
			}
		}
	}

	else err = true;
	
	continue;
}
else{
	err = true;
	continue;
}
		}
	}
}